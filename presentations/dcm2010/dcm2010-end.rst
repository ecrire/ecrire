Utiliser wp, joomla
===================================
...

----

Outils de sélection
===================================
* drupal.org
* drupalmodules.com
* gdo

...

----


Choix difficiles
===================================
* Importer
* Accès
* geo / location

...

----

Questions?
===================================
...

----

Conclusion
===================================
Open atrium, managing news, aegir, etc

Install profiles / drupal distributions

----

Colophon
======================
:Titre: Stratégies de sélection de modules
:Auteur: Robin Millette
:Date: 2010/10/15
:Lieu: Montréal, Québec
:Événement: DrupalCamp Montréal 2010
:Licence: Creative Commons BY-SA 3.0
:URL: http://rym.waglo.com/prez/dcm2010/

À propos de cette présentation:
=================================
:Éditeur: vim
:Entrée: restructuredText (python docutils)
:Sortie: HTML 5 / Javascript / CSS 3
:Convertisseur: Landslide
:Highlight: Pygments
